# cancerradarr - Cancer RADAR project R package.

## Cancer RADAR project

The Cancer RADAR project aims to put infection-related cancer inequalities among individuals with a migration background on the radar, and reduce these inequalities by generating evidence.

**Why?** Migrants in Europe are often disproportionally affected by infection-related cancers. Quantitative descriptions of such health disparities are scarce. These projections will be used to map existing risk and burden disparities among migrants and provide future projections on how to decrease the observed disparities.

**What?** Quantify the infection-related cancer risk of individuals with a migration background and compare to the general population in Europe. 

**How?** Using data from European cancer registries, we aim to provide a unified Europe-wide quantitative overview of the current risk of infection-related cancer risk among individuals with a migration background.

**Definitions:** 
Individuals with a migration background (IMB) = an individual living in another country than he/she is born (born outside of the host country)
Host country = individuals born in the country the cancer registry is situated

## cancerradarr package

The objective of this tool (package) is to facilitate data sharing between cancer registries in Europe and International Association of Cancer Registries (IACR). The tool contains a set of functions that will generate aggregated indicators, for example incidence rates, incidence rate ratios, incidence rate differences. These numbers will serve as input for Cancer RADAR’s project. Please see infographics on the proposed data flow (to be added).

### How does it work

We propose to install the package using the explanation below.

This package will provide you an empty excel-file in which the data can be entered.

This empty excel-file contains different sheets in which the indicated data as totals and in 5-year age-groups can be provided by country-of-birth of the individual. Country-of-birth will serve as proxy for migration background.

Depending on the availability of the data at the cancer-registry the excel-file will provide the opportunity to complete a list of variables.

Type of data collected:

1. number new cancer cases
2. number of death due to the respective cancer
3. population-at-risk*
*Data on the population-at-risk is recommended, but if this data is not available we will use indirect methods to collect the data.

Type of cancer for which data will be collected:
1. Cervical cancer: C53 ???
2. Gastric cancer: ICD9 (1510) and ICD10 (C16), excluding lymphomas ???
3. Liver cancer: IC XXX ???
4. All cancers (this is necessary to estimate the indirect method)


Type of stratification:
1. per calendar-year
2. 5-year age groups for the period 2003-2017

### Installing the package

The package is currently hosted on gitlab.com and can be installed within R just typing.

```r
## check remotes package is installed and install if if not.
if(!('remotes' %in% rownames(installed.packages()))) install.packages('remotes', dep = TRUE)

## install cancerradarr package
remotes::install_gitlab("cancerradar/cancerradarr")
```


### Download empty excel-file (input file) to enter the data

The first step of the workflow is to create an input template file.
`create_registry_input_file()` function has been designed to create a template excel file 
cancer registries should fill in order to produce all the cancer RADAR project summary
statistics. 

The following command will create a file named `cancerRADAR_input.xlsx` on the hard drive 
which will have to be filled with cancer incidence data coming form each cancer registry
of the project.

```r
library(cancerradarr)
create_registry_input_file('cancerRADAR_input.xlsx')
```

### Enter data in the empty input file in excel

1. Open the just created input file and enter the data in the different sheets (for now only incidence data is requested).
2. You will see different empty sheets. The first sheet provides an explanation for the data that needs to be entered. 
3. Save the file under a different file name providing a date and version number (e.g `cancerRADAR_input_completed_xx_xx_2023_vsX.xlsx`)


### Computing Cancer RADAR summary statistics 


Now the data has been entered in the excel file you can create summary statistics using the Cancer RADAR package. The output of this package are the effect measures that will be shared with International Association of Cancer Registries (IACR).

To do this:
1. 

When the cancer registry data have been entered into the input file, user should create the summary
statistics using `create_canradar_summary_file()` function. This function requires 2 files path as 
parameters. The path to the filled template file from the previous step (e.g `cancerRADAR_input_filled.xlsx`)
and the path to an output file where summary statistics will be stored (e.g. `cancerRADAR_output.xlsx`).

To compute the summary statistics just type in R (adapting the filename of the input and output files if needed)

```r
create_canradar_summary_file('cancerRADAR_input_filled.xlsx', 'cancerRADAR_ouput.xlsx')
```

This should in this case create `'cancerRADAR_ouput.xlsx`, a multi tabs excel sheet containing all the summary
statistics needed to the project.

### Sending Cancer RADAR summary statistics 

When you have completed the previous step please send the created file to **describe here the procedure to send data**S
