# cancerradarr (development version)

# cancerradarr 0.0.0.9000

* Added a `NEWS.md` file to track changes to the package.

# cancerradarr 0.0.0.9001

* add example data set to use in documentation

# cancerradarr 0.0.0.9002

* add CI script for package automatic compilation

# cancerradarr 0.0.0.9003

* draft README
