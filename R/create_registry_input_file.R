#' Create a template file to be filled by cancer registry partners
#'
#' @param filename file path, the name of the template file to be created
#'
#' @return
#'  a template .xlsx file is created on the hard drive.
#' @export
#'
#' @examples
#' \dontrun{
#'   create_registry_input_file('cancerRADAR_input.xlsx')
#' }
create_registry_input_file <-
  function(filename = 'cancerRADAR_input.xlsx'){
    ageg.list <- c('total', paste0(sprintf('%02d', seq(0, 70, 5)), '_', sprintf('%02d', seq(4, 74, 5))), '75')
    item.list <- c('npop', 'nallC', 'ncx', 'nliv', 'nstm')
    sex.list <- c('male', 'fmale')
    pop.list <- c('gen', 'migr')

    dat.cob <- NULL
    utils::data('dat.cob', envir = environment())

    dat.list <- list()

    dat.list[['readme']] <-
      dplyr::tribble(
        ~ README,
        paste0('Cancer RADAR input file.'),
        paste0('This file has been created from cancerradarr ', utils::packageVersion('cancerradarr'),
               ' on ', date(), ' by ', Sys.info()[["user"]]),
        paste0('Each tab of this file should be filled with available data from the participating cancer registry.'),
        paste0(''),
        paste0('Tabs short description:'),

        paste0('\t- npop_male_gen: General population at risk (in person-year) per 5-years age group in men.'),
        paste0('\t- nallC_male_gen: General population all cancer cases per 5-years age group in men.'),
        paste0('\t- nliv_male_gen: General population liver cancer cases per 5-years age group in men.'),
        paste0('\t- nsto_male_gen: General population gastric/stomach cancer cases per 5-years age group in men.'),

        paste0('\t- npop_fmale_gen: General population at risk (in person-year) per 5-years age group in women.'),
        paste0('\t- nallC_fmale_gen: General population all cancer cases per 5-years age group in women.'),
        paste0('\t- ncx_fmale_gen: General population cervical cancer cases per 5-years age group in women.'),
        paste0('\t- nliv_fmale_gen: General population liver cancer cases per 5-years age group in women.'),
        paste0('\t- nsto_fmale_gen: General population gastric/stomach cancer cases per 5-years age group in women.'),

        paste0('\t- npop_male_migr: Migant population at risk (in person-year) per 5-years age group and country of birth in men.'),
        paste0('\t- nallC_male_migr: Migrant population all cancer cases per 5-years age group and country of birth in men.'),
        paste0('\t- nliv_male_migr: Migrant population liver cancer cases per 5-years age group and country of birth in men.'),
        paste0('\t- nsto_male_migr: Migrant population gastric/stomach cancer cases per 5-years age group and country of birth in men.'),

        paste0('\t- npop_fmale_migr: Migrant population at risk (in person-year) per 5-years age group and country of birth in women.'),
        paste0('\t- nallC_fmale_migr: Migrant population all cancer cases per 5-years age group and country of birth in women.'),
        paste0('\t- ncx_fmale_migr: Migrant population cervical cancer cases per 5-years age group and country of birth in women.'),
        paste0('\t- nliv_fmale_migr: Migrant population liver cancer cases per 5-years age group and country of birth in women.'),
        paste0('\t- nsto_fmale_migr: Migrant population gastric/stomach cancer cases per 5-years age group and country of birth in women.'),

        paste0(''),
        paste0('If you have no information for a given cancer/country of birth just let the row empty.'),
        paste0('Put some 0s only for true zeros (population at risk exists but no cancer registered), this information will be filter out in the summary statistics computation step.'),

        paste0(''),
        paste0('This file should NOT be transmitted')
      )

    for(pop_ in pop.list){
      for(sex_ in sex.list){
        for(item_ in item.list){
          if(!(sex_ == 'male' & item_ == 'ncx')){
            dat.list[[paste(item_, sex_, pop_, sep = '_')]] <-
              if(pop_ == 'migr'){
                cbind(dat.cob,  paste(item_, sex_, ageg.list, sep = '_') %>% purrr::map_dfc(stats::setNames, object = list(character(1))))
              } else {
                paste(item_, sex_, ageg.list, sep = '_') %>% purrr::map_dfc(stats::setNames, object = list(character(1)))
              }
          }
        }
      }
    }
    if(!file.exists(filename)){
      writexl::write_xlsx(dat.list, path = filename)
      cat('\n>', filename, 'has been created, please fill it with cancer registry data')
    } else {
      warning(paste0(filename, ' already exists on your hard drive. Nothing has been done. ',
                      'If you are sure to want to overwrite this file please type unlink("', filename, '") ',
                      'and rerun the create_registry_input_file() function.'))
    }
  }
