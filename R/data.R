#' Countries label and countries codes
#'
#' A 2 column dataset containing all the countries of birth (with associated countries codes) included
#' in Cancer RADAR project
#'
#' @format ## `dat.cob`
#' A data frame with 185 rows and 60 columns:
#' \describe{
#'   \item{cob_country_of_birth}{Country name}
#'   \item{cob_code}{Country code}
#'   ...
#' }
"dat.cob"

#
# #' Example of Cancer RADAR filled input data
# #'
# #' A .xlsx fille generated with create_registry_input_file() and filled with fake data to be
# #' used in examples.
# #'
# #'
# #' @format ## `ex_cancerRADAR_input_filled.xlsx"`
# #' A data frame with 185 rows and 60 columns:
# #' \describe{
# #'   \item{cob_country_of_birth}{Country name}
# #'   \item{cob_code}{Country code}
# #'   ...
# #' }
# "ex_cancerRADAR_input_filled"
#
